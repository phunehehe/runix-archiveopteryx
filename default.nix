{ config, exports, home, lib, pkgs, ... }:

let

  inherit (lib) catAttrs head mapAttrsToList mkOption optionalString;
  inherit (lib.types) int nullOr str;

  jail-directory = "${home}/jail";
  message-copy-directory = "${jail-directory}/messages";
  socket-directory = "${jail-directory}/var/run";
  tls-certificate = "${home}/key-and-cert.pem";

  # These are hardcoded in
  # https://github.com/aox/aox/blob/21b8caa8140f80f0b967f79f5984e3d72a10ecff/Jamsettings
  aox-conf = "/usr/local/archiveopteryx/archiveopteryx.conf";
  pidfile-dir = "/usr/local/archiveopteryx/lib/pidfiles";

  le-dir = head
    (pkgs.collectAttrByPath ["letsencrypt" "certs" config.domain] exports);

  hostname = optionalString (config.hostname != null)
    "hostname = ${config.hostname}";

  # TODO: what about aoxsuper.conf?
  real-aox-conf = pkgs.writeText "archiveopteryx.conf" ''
    allow-plaintext-access = localhost
    db-name = ${config.db-name}
    db-password = ${config.db-password}
    db-user = ${config.db-user}
    ${hostname}
    jail-directory = ${jail-directory}
    lmtp-port = ${toString config.lmtp-port}
    logfile = -
    message-copy = errors
    message-copy-directory = ${message-copy-directory}
    tls-certificate = ${tls-certificate}
    use-imaps = true
    use-subaddressing = true
  '';

  processes = {

    archiveopteryx = pkgs.writeBash "archiveopteryx" ''

      PATH=${lib.makeBinPath [
        pkgs.coreutils
      ]}

      {
        # This trick deals with newlines at the end of certificate files, which
        # might be missing
        printf '%s\n' "$(cat ${le-dir}/key.pem)"
        printf '%s\n' "$(cat ${le-dir}/fullchain.pem)"
      } > ${tls-certificate}
      chmod go-rwx ${tls-certificate}

      exec ${pkgs.archiveopteryx}/bin/archiveopteryx -c ${aox-conf}
    '';

    # Cannot drop privileges because logd chroot later
    logd = "${pkgs.archiveopteryx}/bin/logd -c ${aox-conf}";
  };

  le-renew-hook = ''
    # The server will be started again by something
    ${pkgs.archiveopteryx}/bin/aox stop
  '';

in {

  exports = {
    inherit (config) lmtp-port;
    letsencrypt.renew-hooks.${config.domain} = le-renew-hook;
  };

  options = {
    db-name = mkOption {
      type = str;
      default = "archiveopteryx";
    };
    db-user = mkOption {
      type = str;
      default = "aox";
    };
    db-password = mkOption {
      type = str;
    };
    domain = mkOption {
      type = str;
    };
    hostname = mkOption {
      type = nullOr str;
      default = null;
    };
    lmtp-port = mkOption {
      type = int;
      # FIXME: it's not great to repeat the default, but this is required for
      # exporting. What's a better way?
      default = 2026;
    };
  };

  run = pkgs.writeBash "archiveopteryx" ''

    PATH=${lib.makeBinPath [
      pkgs.coreutils
      pkgs.shadow
    ]}

    # logd wants owner and group and permissions
    cp --remove-destination ${real-aox-conf} ${aox-conf}
    chown aox:aox ${aox-conf}
    chmod 440 ${aox-conf}

    mkdir --parents ${pidfile-dir}
    mkdir --parents ${socket-directory}

    mkdir --parents ${jail-directory}
    chmod 701 ${jail-directory}

    mkdir --parents ${message-copy-directory}
    chown aox ${message-copy-directory}

    exec ${pkgs.runsvdir home processes};
  '';
}
