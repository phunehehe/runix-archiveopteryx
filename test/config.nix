let
  domain = "example.com";
in {
  archiveopteryx = {
    inherit domain;
    db-password = "secret";
  };
  le-dummy = {
    inherit domain;
  };
}
