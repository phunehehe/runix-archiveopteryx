#!/usr/bin/env bash
set -efuxo pipefail

NEW_PATH=''
for p in coreutils git gnused nix rsync
do
  NEW_PATH=$(nix-build --no-out-link '<nixpkgs>' --attr $p)/bin:$NEW_PATH
done
PATH=$NEW_PATH

update_submodules() {
    directory="$1"
    git -C "$directory" submodule sync
    git -C "$directory" submodule update --init --recursive
}

fetch() {
    repository="$1"
    directory="$2"
    branch="${3:-master}"
    if [[ -e "$directory" ]]
    then
        git -C "$directory" fetch
        git -C "$directory" checkout "origin/$branch"
        update_submodules "$directory"
    else
        git clone --recursive --branch "$branch" "$repository" "$directory"
    fi
}

src_dir=$(readlink -f "$(dirname "$0")/..")
fetch https://gitlab.com/phunehehe/runix.git $src_dir/test/runix
update_submodules "$src_dir"
nix-build --no-out-link "$src_dir/test/runix"
